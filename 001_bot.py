import numpy as np
import matplotlib as mpl
import requests, json
import pause
import os
import query
import random
import keras
from datetime import datetime, timedelta
from log import log, log2
from account import Account

INPUT_SIZE = 1
WARM_UP = 100
SLEEP = 0.05
EPOCHS = 32
TOLERANCE = 0.0000001
LSTM_UNIT = 128
MODEL = 2
DROPOUT = 0
ACTIVATION = 'tanh'
OPTIMIZER = keras.optimizers.Adam(lr=0.001)
LOSS = 'sparse_categorical_crossentropy'

scores = [0.0]

for x in range(0, 1000):

    account = Account(euros=1000.0, bitcoins=0.0)
    query.initSourceFile()

    """
    INPUT_SIZE = random.choice([1, 2, 4, 8, 12, 16, 20, 24, 30, 35, 40])
    EPOCHS = random.choice([1, 2, 4, 8, 16, 32, 40, 50, 65, 80, 100])
    LSTM_UNIT = random.choice([1, 2, 4, 8, 16, 32, 64, 128])
    MODEL = random.choice([2])
    DROPOUT = random.choice([0, 0.1, 0.2])
    ACTIVATION = random.choice(['none', 'tanh'])
    OPTIMIZER = random.choice([keras.optimizers.SGD(lr=0.01, nesterov=True),
        keras.optimizers.Adam(lr=0.001),
        keras.optimizers.Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004),
        keras.optimizers.Adamax(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0),
        keras.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0),
        keras.optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0),
        keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)]) 
    LOSS = random.choice(['mean_squared_error',
        'mean_absolute_error',
        'mean_absolute_percentage_error',
        'mean_squared_logarithmic_error',
        'squared_hinge',
        'hinge',
        'categorical_hinge',
        'logcosh',
        'sparse_categorical_crossentropy',
        'binary_crossentropy',
        'kullback_leibler_divergence',
        'poisson',
        'cosine_proximity'])
    """

    log2("INPUT_SIZE = " + str(INPUT_SIZE))
    log2("WARM_UP = " + str(WARM_UP))
    log2("SLEEP = " + str(SLEEP))
    log2("EPOCHS = " + str(EPOCHS))
    log2("TOLERANCE = " + str(TOLERANCE))
    log2("LSTM_UNITS = " + str(LSTM_UNIT))
    log2("MODEL = " + str(MODEL))
    log2("DROPOUT = " + str(DROPOUT))
    log2("ACTIVATION = " + str(ACTIVATION))
    log2("OPTIMIZER = " + str(OPTIMIZER))
    log2("LOSS = " + str(LOSS))

    model = keras.Sequential()
    if (MODEL == 1):
        model.add(keras.layers.LSTM(LSTM_UNIT, input_shape=(INPUT_SIZE, 1)))
    elif (MODEL == 2):
        model.add(keras.layers.LSTM(LSTM_UNIT, input_shape=(INPUT_SIZE, 1), return_sequences=True))
        model.add(keras.layers.LSTM(LSTM_UNIT, return_sequences=False))

    if (DROPOUT > 0):
        model.add(keras.layers.Dropout(DROPOUT))

    model.add(keras.layers.Dense(1))

    if (ACTIVATION != 'none'):
        model.add(keras.layers.Activation(ACTIVATION))

    model.compile(optimizer=OPTIMIZER, loss=LOSS)

    datain = np.zeros((1, INPUT_SIZE, 1), dtype='d')
    output = np.array([0], dtype='d')

    i = 0
    npredictions = 0
    successes = 0
    previousPrice = 1.0
    previousPrediction = 0.0
    totalGap = 0.0

    for x in range(0, 480):
        
        now = datetime.now()
        log("")
        log("ITERATION #" + str(i) + " WALLET : " + str(account.euros) + " eur, " + str(account.bitcoins) + " btc")
        log("  | Time : " + now.strftime('%Y-%m-%d %H:%M:%S'))

        # Get current price from internet
        price = query.getBitcoinEuroPremade()
        delta = price / previousPrice - 1
        output[0] = delta

        # Fit data if it has enough data
        if (i > INPUT_SIZE):
            model.fit(datain, output, epochs = EPOCHS, shuffle = False, verbose = 0)
            log("  | Fitting...")
        else:
            log("  | Initializing...")

        # Roll the input data for next iteration
        datain = np.roll(datain, -1, axis=1)
        datain[0][-1][0] = delta
        previousPrice = price

        log("  | Last delta : " + str(100.0 * delta) + "%")

        # Real stuff
        if (i > INPUT_SIZE + WARM_UP):
            if (npredictions > 0):
                gap = abs(previousPrediction - delta)
                totalGap = totalGap + gap
                log("    | Gap : " + str(gap))
                log("    | Mean gap : " + str(totalGap / npredictions))
                if (previousPrediction > TOLERANCE):
                    if (delta > 0):
                        successes = successes + 1
                        log("  | Previous move : Success !")
                    else:
                        log("  | Previous move : Failure !")
                elif (previousPrediction < -TOLERANCE):
                    if (delta < 0):
                        successes = successes + 1
                        log("  | Previous move : Success !")
                    else:
                        log("  | Previous move : Failure !")
                else:
                    if (delta > -TOLERANCE * 2 and delta < TOLERANCE * 2):
                        successes = successes + 1
                        log("  | Previous move : Success !")
                    else:
                        log("  | Previous move : Failure !")

                log("    | Success rate : " + str(100.0 * successes / npredictions) + "%")
            
            prediction = model.predict(datain)[0][0]

            if (prediction > TOLERANCE):
                log("  | Next delta : " + str(100.0 * prediction) + "% (" + str(price) + " to " + str((1.0 + prediction) * price) + ")") 
                account.buy(price)
            elif (prediction < -TOLERANCE):
                log("  | Next delta : " + str(100.0 * prediction) + "% (" + str(price) + " to " + str((1.0 + prediction) * price) + ")") 
                account.sell(price)
            else:
                log("  | Next delta : ~0%") 

            previousPrediction = prediction
            npredictions = npredictions + 1
            
        elif (i > INPUT_SIZE):
                log("  | Warming-up...")   

        # Next iteration
        i = i + 1
        pause.until(now + timedelta(seconds=SLEEP))

    score = 100.0 * successes / npredictions
    scores.append(score)
    maxScore = max(scores)
    minScore = min(scores)

    account.sell(price)
    log2("-> Euros: " + str(account.euros))
    log2("-> Sell/Buy: " + str(score))
    log2("-> Mean Gap: " + str(totalGap / npredictions))

    if (score == maxScore):
        log2("-> Best Score !")

    if (score == minScore):
        log2("-> Worst Score !")