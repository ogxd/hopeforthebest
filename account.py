from log import log

class Account:

    def __init__(self, euros, bitcoins):
        self.euros = euros
        self.bitcoins = bitcoins

    def buy(self, price):
        if (self.euros < 0.1):
            log("    | Can't buy bitcoins, not enough euros")
        else:
            self.bitcoins = self.euros / price
            self.euros = 0
            log("    | Buying " + str(self.bitcoins) + " btc at " + str(price) + " eur/btc")

    def sell(self, price):
        if (self.bitcoins < 0.001):
            log("    | Can't sell bitcoins, not enough bitcoins")
        else:
            log("    | Selling " + str(self.bitcoins) + " btc at " + str(price) + " eur/btc")
            self.euros = self.bitcoins * price
            self.bitcoins = 0